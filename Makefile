
clean:
	rm -rf build dist sitemaker.egg-info

install:
	python3 setup.py install

dist:
	python3 setup.py sdist
