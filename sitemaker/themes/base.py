from sitemaker import *
class theme_css:

    def __init__(self):
        self.css=None
        self.navbar_background="white"
        self.navbar_color="black"
        self.header_background="white"
        self.footer_background="white"
        self.body_color="white"
        self.text_color="black"
        self.main_background="white"
        self.accent="black"
        
    def build(self):
        self.css=cssfile()
        return self.css.build()

    def save(self,path):
        with open(path,"w") as file:
           file.write(self.build())
        
class theme_page:

    def __init__(self):
        self.p=page()
        self.logo=image()
        self.menu=[]
        self.sections=[]
        self.title=""
        self.footers=[]
        
    def build(self):
        c=content()
        f=tagged("footer")
        nav=navbar("h")
        for n in self.menu:
            nav.addItem(n)
        c.addItem(nav)
        for n in self.sections:
            c.addItem(n)
        for n in self.footers:
            f.addItem(n)
        self.p.content=c
        self.p.footer=f
        return self.p.build()
    
    def add(self,item):
        self.sections.append(item)
        
    def addFooter(self,item):
        self.footers.append(item)

    def addCss(self,name):
        self.p.addRel(name)
        
    def save(self,path="index.html"):
        with open(path,"w") as file:
            file.write(self.build())
