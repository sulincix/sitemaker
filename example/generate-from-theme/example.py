from sitemaker import *
from sitemaker.themes import base as theme

z=theme.theme_page()
zs=theme.theme_css()
zs.save("example.css")
z.title="example"
z.logo=image("image.png")
z.menu.append(item("example menu item"))
z.addFooter(item("example footer"))
z.add(item("hello world"))
z.p.addRel("example.css")
z.save("index.html")
