import setuptools
long_description = ""
setuptools.setup(
     name='sitemaker',  
     version='0.4.1',
     scripts=[] ,
     author="sulincix",
     author_email="aa@aa.aa",
     description="A site maker",
     long_description=long_description,
   long_description_content_type="text/markdown",
     url="https://gitlab.com/sulincix/sitemaker",
     packages=['sitemaker', 'sitemaker/themes'],
     
 )
